<?php namespace BOFHBot\Commands;

use PhpSlackBot\Command\BaseCommand;

class Excuse extends BaseCommand {
    
    protected static $excuses = [];
    
    protected function configure() {
        $this->setName('excuse');
        self::$excuses = explode("\n", file_get_contents(__DIR__.'/../../resources/excuses.txt'));
    }
    
    protected function execute($message, $context) {
        $this->send($this->getCurrentChannel(), $this->getCurrentUser(), self::$excuses[array_rand(self::$excuses)]);
    }
}
